#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from bs4 import BeautifulSoup
import getpass
from twill.commands import fv, submit
from web_parser import get_link_by_text, go_to, to_float, to_int, get_html


def read_grades(user, password):

    go_to('https://vw-online.hdm-stuttgart.de/qisserver')

    try:
        fv("1", "asdf", user)
        fv("1", "fdsa", password)
        submit('0')
    except:
        pass  # when you already have the cookie

    link = get_link_by_text(r'Pr.fungsverwaltung')
    go_to(link)

    link = get_link_by_text(r'Notenspiegel')
    go_to(link)

    # parse

    soup = BeautifulSoup(get_html())

    grades = []

    for tr in soup.find_all("tr"):
        tds = tr.find_all("td")
        if len(tds) > 0:
            if tds[0].get_text().isdigit():
                grades.append({
                    "number": tds[0].get_text().strip(),
                    "subjetc": tds[1].get_text().strip(),
                    "name": tds[2].get_text().strip(),
                    "semester": tds[4].get_text().strip(),
                    "grade": to_float(tds[5].get_text()),
                    "result": tds[6].get_text().strip(),
                    "ects": to_int(tds[7].get_text().strip())
                })
    return grades


def main():
    print """

        HdM Stuttgart Grade Grapper

        This is a utility to display your HdM grades and ECTS
            by Thomas Uhrig (www.tuhrig.de)

    """
    user = raw_input("Enter your HdM name (e.g. XY007): ")
    pwd = getpass.getpass()

    print ""
    print "Loading..."
    print ""

    grades = read_grades(user, pwd)

    print ""
    print "*******************************************************"
    print "RESULTS"
    print "*******************************************************"
    print ""

    for grade in grades:
        if "angemeldet" in grade["result"]:
            print grade["name"].ljust(30) + " \t- " + "pending...".ljust(10)
        else:
            print grade["name"].ljust(30) + " \t- " + str(grade["ects"]).ljust(10) + "ECTS - " + str(grade["grade"])

    total_ects = 0
    total_courses = 0
    total_pending = 0
    total_finished = 0
    total_grade = 0
    total_graded_ects = 0
    total_graded_courses = 0
    for grade in grades:

        total_courses += 1
        if "angemeldet" not in grade["result"]:
            total_ects += grade["ects"]
            total_finished += 1

            try:
                ects_float = grade["ects"]
                grade_float = grade["grade"]
                total_grade += ects_float * grade_float
                total_graded_ects += ects_float
                total_graded_courses += 1
            except:
                pass
        else:
            total_pending += 1

    print ""
    print str(total_ects) + " total ECTS"
    print str(total_courses) + " courses (" + str(total_finished) + " finished, " + str(total_pending) + " pending)"
    print str(total_graded_ects) + " graded ECTS points from " + str(total_graded_courses) + " courses"
    print str(total_grade / total_graded_ects) + " overall grade"

    print ""

    print str(120 - total_ects) + " missing to a 120 ETCS master  degree (= 4 semester)"
    print "worst case: " + str((((120 - total_ects) * 4.0 + total_grade) / 120)) + " with " + str(120 - total_ects) + " ECTS with 4.0"
    print "best case: " + str((((120 - total_ects) * 1.0 + total_grade) / 120)) + " with " + str(120 - total_ects) + " ECTS with 1.0"
    print ""

    print str(180 - total_ects) + " missing to a 180 ETCS bachelor degree (= 6 semester)"
    print "worst case: " + str((((180 - total_ects) * 4.0 + total_grade) / 180)) + " with " + str(180 - total_ects) + " ECTS with 4.0"
    print "best case: " + str((((180 - total_ects) * 1.0 + total_grade) / 180)) + " with " + str(180 - total_ects) + " ECTS with 1.0"
    print ""

    print str(210 - total_ects) + " missing to a 210 ETCS bachelor degree (= 7 semester)"
    print "worst case: " + str((((210 - total_ects) * 4.0 + total_grade) / 210)) + " with " + str(210 - total_ects) + " ECTS with 4.0"
    print "best case: " + str((((210 - total_ects) * 1.0 + total_grade) / 210)) + " with " + str(210 - total_ects) + " ECTS with 1.0"
    print ""


if __name__ == "__main__":
    main()
