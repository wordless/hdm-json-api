#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from bs4 import BeautifulSoup
import twill
import getpass
import StringIO
from twill.commands import add_auth
from web_parser import go_to, to_int, get_html


def read_schedule(user, password):

    a = twill.commands
    a.config("with_default_realm", 1)

    add_auth("hdm-stuttgart", "https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan", user, password)

    go_to("https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan")

    soup = BeautifulSoup(get_html())

    table = soup.find("table")

    # table is undefined if there is not course selected
    courses = []
    if table:
        tr_list = table.find_all("tr")
        for tr in tr_list[1:]:
            td_list = tr.find_all("td")
            courses.append({
                "date": td_list[0].get_text().strip(),
                "id": td_list[1].get_text().strip(),
                "name": td_list[2].find("a").get_text().strip(),
                "start": td_list[3].get_text().strip(),
                "end": td_list[4].get_text().strip(),
                "room": td_list[5].get_text().strip(),
                "ects": to_int(td_list[6].get_text()),
                "hours": to_int(td_list[7].get_text()),
                "link": "https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan/" + td_list[2].find("a")['href'],
                "remove": "https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan/" + td_list[8].find("a")['href']
            })

    return courses


def main():

    print """

        HdM Stuttgart Schedule Grapper

        This is a utility to display your HdM schedule
            by Thomas Uhrig (www.tuhrig.de)

    """
    user = raw_input("Enter your HdM name (e.g. XY007): ")
    pwd = getpass.getpass()

    print ""
    print "Loading..."
    print ""

    print read_schedule(user, pwd)


if __name__ == "__main__":
    main()
