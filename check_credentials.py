#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

import twill
import getpass
from twill.commands import add_auth
from web_parser import go_to, get_html


def check_credentials(user, password):

    a = twill.commands
    a.config("with_default_realm", 1)

    add_auth("hdm-stuttgart", "https://www.hdm-stuttgart.de/intranet", user, password)

    go_to("https://www.hdm-stuttgart.de/intranet")

    if "Willkommen im Intranet der Hochschule der Medien" in get_html():
        return True;
    return False;


def main():

    print """

        HdM Stuttgart Calendar Grapper

        This is a utility to check HdM credentials
            by Thomas Uhrig (www.tuhrig.de)

    """

    user = raw_input("Enter your HdM name (e.g. XY007): ")
    pwd = getpass.getpass()

    print ""
    print "Loading..."
    print ""

    print check_credentials(user, pwd)


if __name__ == "__main__":
    main()
