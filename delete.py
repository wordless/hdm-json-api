#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from twill.commands import add_auth
from web_parser import go_to


def delete(user, password, person, course):

    add_auth("hdm-stuttgart", "https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan", user, password)
    link = "https://www.hdm-stuttgart.de/studenten/stundenplan/pers_stundenplan/index_html?pers_stundenplan_delete=1&person_ID=" + person + "&sgblock_vorlesung_semester_ID=" + course
    go_to(link)
