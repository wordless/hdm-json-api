from bs4 import BeautifulSoup
from twill.commands import go, get_browser
import re
import twill


browser = get_browser()
a = twill.commands
a.config("with_default_realm", 1)


def get_html():
    return browser.get_html()


def go_to(link):
    print "load " + link
    go(link)


def to_int(string):
    string = string.strip()
    if string == "":
        return None
    return int(string)


def to_float(string):
    string = string.strip().replace(",", ".")
    if string == "":
        return None
    return float(string)


def get_link_by_text(link_name):
    soup = BeautifulSoup(browser.get_html())
    pattern = re.compile(link_name)
    link_tag = soup.find('a', text=pattern)
    return link_tag.get('href')
