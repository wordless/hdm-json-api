#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from bs4 import BeautifulSoup
import twill
from web_parser import go_to, get_html

DAYS = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday"
        ]


def read_canteen():

    a = twill.commands
    a.config("with_default_realm", 1)

    go_to("http://www.studentenwerk-stuttgart.de/gastronomie/speiseangebot")

    soup = BeautifulSoup(get_html())

    days = {}

    for index, table in enumerate(soup.find_all("table")):
        day = []
        tds = table.findAll(attrs={'class': "speiseangebotbody"})
        for td in tds:
            string = str(td)
            string = string.replace("</span><br/>", "</span><p class='price'>")
            string = string.replace("<br/><br/>", "</p></td>")
            item = BeautifulSoup(string)
            if item.find(attrs={'class': "name"}):
                day.append({
                        "name": item.find(attrs={'class': "name"}).get_text().strip(),
                        "price": item.find(attrs={'class': "price"}).get_text()[:11].strip()
                        })
        days[DAYS[index]] = day
    return days


def main():

    print """

        HdM Stuttgart Canteen Grapper

        This is a utility to display the Stuttgart canteen
            by Thomas Uhrig (www.tuhrig.de)

    """

    print ""
    print "Loading..."
    print ""

    print read_canteen()


if __name__ == "__main__":
    main()
