#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from bs4 import BeautifulSoup
import twill
import getpass
from twill.commands import add_auth
from web_parser import go_to, get_html


def read_calendar(user, password):

    a = twill.commands
    a.config("with_default_realm", 1)

    add_auth("hdm-stuttgart", "https://www.hdm-stuttgart.de/intranet/terminkalender", user, password)

    go_to("https://www.hdm-stuttgart.de/intranet/terminkalender")

    soup = BeautifulSoup(get_html())

    table = soup.find("table")

    tr_list = table.find_all("tr")

    events = []

    for tr in tr_list[1:]:
        td_list = tr.find_all("td")
        if len(td_list) is 2:
            events.append({
                "date": td_list[0].get_text().strip(),
                "event": td_list[1].get_text().replace("[In Outlook eintragen]", "").strip(),
                "link": "https://www.hdm-stuttgart.de/intranet/terminkalender/" + td_list[1].find("a")['href']
            })
    return events


def main():

    print """

        HdM Stuttgart Calendar Grapper

        This is a utility to display the HdM calendar
            by Thomas Uhrig (www.tuhrig.de)

    """

    user = raw_input("Enter your HdM name (e.g. XY007): ")
    pwd = getpass.getpass()

    print ""
    print "Loading..."
    print ""

    for event in read_calendar(user, pwd):
        print event


if __name__ == "__main__":
    main()
