#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from bs4 import BeautifulSoup
import getpass
from twill.commands import add_auth
from web_parser import go_to, get_html


def search(user, password, query, start=0):

    add_auth("hdm-stuttgart", "https://www.hdm-stuttgart.de/intranet/new_search", user, password)
    go_to("https://www.hdm-stuttgart.de/intranet/new_search")
    go_to("https://www.hdm-stuttgart.de/intranet/new_search?query=" + str(query) + "&start=" + str(start))

    soup = BeautifulSoup(get_html())
    content = soup.find(id="center_content")
    p_list = content.find_all("p")

    results = []
    for p in p_list[2:12]:
        print p
        results.append({
                    "name": p.find("a").get_text().strip(),
                    "link": p.find("a")["href"],
                    "text": p.get_text().strip()
                })

    return results


def main():

    print """

        HdM Stuttgart Search Grapper

        This is a utility to use the HdM search
            by Thomas Uhrig (www.tuhrig.de)

    """
    user = raw_input("Enter your HdM name (e.g. XY007): ")
    pwd = getpass.getpass()
    query = raw_input("Enter a search term: ")

    print ""
    print "Loading..."
    print ""

    print search(user, pwd, query)


if __name__ == "__main__":
    main()
