HdM Web API
===========

This project provides a simple web API to access individual student related data (e.g. your personal schedule or your grades) as JSON. On top of this API it offers a web interface to view this data easily (e.g. on our desktop PC or your mobile device).

# Installation #

- Check out the repository: `git clone https://bitbucket.org/wordless/hdm-json-api.git`
- Install the `requirements.txt` which you can find in the root folder: `pip install -r requirements.txt`
- Start the Flask application: `python app.py`
- Open your browser and go to `http://127.0.0.1:5000/` and follow the instructions

# API Examples & Caching#

All API calls are cached using the Flask SimpleCache. But you can easily switch to memcache or something similar. Each API call is cached for a different time span depending on how much the specific content will change. You can easily change this constants in the `app.py`.

    CACHE_CALENDAR = 12 * 60 * 60 # seconds
    CACHE_GRADES = 60 # seconds
    CACHE_CANTEEN = 24 * 60 * 60 # seconds
    CACHE_SCHEDULE = 5 * 60 # seconds
    CACHE_SEARCH = 12 * 60 * 60 # seconds

The delete call to delete courses from your personal schedule is not cached.

- Grades:
[http://127.0.0.1:5000/grades](http://127.0.0.1:5000/grades)
- Schedule:
[http://127.0.0.1:5000/schedule](http://127.0.0.1:5000/schedule)
- Calendar:
[http://127.0.0.1:5000/calendar](http://127.0.0.1:5000/calendar)
- Canteen:
[http://127.0.0.1:5000/canteen](http://127.0.0.1:5000/canteen)
- Search:
[http://127.0.0.1:5000/search?query=test&start=0](http://127.0.0.1:5000/search?query=test&start=0)
- Delete:
[http://127.0.0.1:5000/delete?link=link](http://127.0.0.1:5000/delete?link=link)

# Libraries #

- [Flask](http://flask.pocoo.org/) as web framework
- [Ractive.js](http://www.ractivejs.org/) as front-end framework
- [twill](http://twill.idyll.org/) and [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/) to parse websites
- [Pure.css](http://purecss.io/) for design
- [simple-statistics](https://github.com/tmcw/simple-statistics) for calulations in the UI