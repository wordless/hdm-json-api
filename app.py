from flask import Flask, jsonify, session, redirect, url_for, escape, request
from read_grades import read_grades
from read_schedule import read_schedule
from read_calendar import read_calendar
from read_canteen import read_canteen
from check_credentials import check_credentials
from search import search
from delete import delete
import StringIO
import twill
from werkzeug.contrib.cache import SimpleCache


cache = SimpleCache()
app = Flask(__name__)


CACHE_CALENDAR = 12 * 60 * 60 # seconds
CACHE_GRADES = 60 # seconds
CACHE_CANTEEN = 24 * 60 * 60 # seconds
CACHE_SCHEDULE = 5 * 60 # seconds
CACHE_SEARCH = 12 * 60 * 60 # seconds


# secret session key (for development)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

# config twill to write the log into a StringIO object
output = StringIO.StringIO()
twill.set_output(output)


@app.route('/')
def index():
    if 'username' in session:
        return redirect(url_for('static', filename='ui.html'))
    return redirect(url_for('static', filename='login.html'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if check_credentials(username, password):
            session['username'] = username
            session['password'] = password
            return redirect(url_for('index'))
    return "username or password is incorrect"


@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    session.pop('password', None)
    return redirect(url_for('index'))


@app.route('/about')
def get_about():
    about_text = """
    This is a web API to access our individual student related
    data from the HdM Stuttgart as JSON
    """
    return jsonify(about=about_text)


@app.route('/calendar')
def get_calendar():
    if 'username' in session and 'password' in session:
        username = escape(session['username'])
        password = escape(session['password'])
        key = '#calendar'
        events = cache.get(key)
        if events is None:
            events = read_calendar(username, password)
            cache.set(key, events, timeout=CACHE_CALENDAR)
        return jsonify(events=events)
    return 'You are not logged in'


@app.route('/grades')
def get_grades():
    if 'username' in session and 'password' in session:
        username = escape(session['username'])
        password = escape(session['password'])
        key = username + '#grades'
        grades = cache.get(key)
        if grades is None:
            grades = read_grades(username, password)
            cache.set(key, grades, timeout=CACHE_GRADES)
        return jsonify(grades=grades)
    return 'You are not logged in'


@app.route('/canteen')
def get_canteen():
    key = '#canteen'
    menu = cache.get(key)
    if menu is None:
        menu = read_canteen()
        cache.set(key, menu, timeout=CACHE_CANTEEN)
    return jsonify(menu=menu)


@app.route('/schedule')
def get_schedule():
    if 'username' in session and 'password' in session:
        username = escape(session['username'])
        password = escape(session['password'])
        key = username + '#schedule'
        schedule = cache.get(key)
        if schedule is None:
            schedule = read_schedule(username, password)
            cache.set(key, schedule, timeout=CACHE_SCHEDULE)
        return jsonify(schedule=schedule)
    return 'You are not logged in'


@app.route('/search')
def do_search():
    if 'username' in session and 'password' in session:
        username = escape(session['username'])
        password = escape(session['password'])
        query = request.args.get("query", "")
        start = request.args.get("start", "0")
        key = query + '#' + start + '#search'
        results = cache.get(key)
        if results is None:
            results = search(username, password, query, start)
            cache.set(key, results, timeout=CACHE_SEARCH)
        return jsonify(results=results)
    return 'You are not logged in'


@app.route('/delete')
def do_delete():
    if 'username' in session and 'password' in session:
        username = escape(session['username'])
        password = escape(session['password'])
        person = request.args.get("person_ID", "")
        course = request.args.get("sgblock_vorlesung_semester_ID", "")
        delete(username, password, person, course)
        schedule = read_schedule(username, password)
        return jsonify(schedule=schedule)
    return 'You are not logged in'


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
